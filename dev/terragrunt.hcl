#

remote_state  {  
    backend = "s3"  
    config = {    
                encrypt        = true    
	        bucket         = "djterraform-up-and-running-state"    
                key            = "./terraform.tfstate"    
                region         = "us-east-1"  
                dynamodb_table = "secops-DB"
            }
    }
